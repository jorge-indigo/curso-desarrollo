## Git
Comandos | Descripción
--- | ---
git clone **_url_** | Descarga el proyecto y genera una carpeta con el nombre del mismo
git status | Muestra el estatus de los archivos en sus tres fases (untracked, stage, committed)
git add  **_archivo1_** **_archivo2_** ... **_archivoN_** | Mueve los archivos a la fase _stage_
git commit **_archivo1_** **_archivo2_** ... **_archivoN_** -m _"comentario"_ | Crea un snapshot de los archivos y lo guarda en la base de datos local
git push **_origin_** **_master_** | Sube los archivos al repositorio (github o bitbucket) 
