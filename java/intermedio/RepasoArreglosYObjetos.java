public class RepasoArreglosYObjetos {
  public static void main(String [] args) {
    int [] numeritos = new int[3];

    int variable1 = 10;
    int variable2 = 20;
    int variable3 = 30;

    numeritos[0] = 10;
    numeritos[1] = 20;
    numeritos[2] = 30;

    Object [] objetitos = new Object[100];
    Persona [] personitas = new Persona[10];

    // tipo null
    Persona tania = new Persona("Tania", "Ruiz", 23);
    //tania = null;
    tania.saluda(); // ERROR

    personitas[0] = tania;

    personitas[0].saluda();

    personitas[1] = new Persona("Samantha", "Ruiz", 24);
    personitas[1].saluda();
  }
}
