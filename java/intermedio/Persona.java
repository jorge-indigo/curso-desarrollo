public class Persona {

  // Atributos de la clase
  // Nombre (String)
  // Apellido Paterno (String)
  // Apellido Materno (String)
  // Edad (int)
  // Peso (double)
  // Estatura (double)
  // Sexo (char)
  // Color (String)
  // [accesibilidad] [tipo] [nombre];
  public String nombre;
  public String apellidoPaterno;
  public String apellidoMaterno;
  public int edad;
  public double peso;
  public double estatura;
  public char sexo;
  public String color;

  public Persona(String nombre, String apellidoPaterno, int edad) {
    this.nombre = nombre;
    this.apellidoPaterno = apellidoPaterno;
    this.edad = edad;
  }

  // Métodos
  // Comer
  // Bañarse
  // Dormir
  // Jugar
  // Morirse
  // Caminari
  // [accesibilidad] [tipo de retorno] [nombre]( [parametros ] )
  public void morirse() {
    System.out.println("Me morí x.x");
  }

  public void comer(String comida) {
    System.out.println("Estoy comiendo " + comida);
  }

  public void saluda() {
    System.out.println("Hola, me llamo " + this.nombre);
  }

  public static void main(String [] args) {

    // Crear una instancia de un objeto
    // [tipo] [nombre de variable] = new [tipo]( parametros );
    // Scanner entrada = new Scanner(System.in);

    Persona tania = new Persona("Tania", "Ruiz", 22);
    Persona alan = new Persona("Alan", "Ortiz", 24);
    tania.saluda();
  }
}
