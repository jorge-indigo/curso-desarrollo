public class Racional {

  int numerador;
  int denominador;

  /*
   * Racional r = new Racional(1,2);
   */
  public Racional(int numerador, int denominador) {
    this.numerador = numerador;
    this.denominador = denominador;
  }

  // a/b + c/d = (ad  + bc)/bd
  public Racional sumar(Racional r) {
    int a = this.numerador;
    int b = this.denominador;
    int c = r.numerador;
    int d = r.denominador;

    int nuevoNumerador = (a*d) + (b*c);
    int nuevoDenominador = b * d;

    return new Racional(nuevoNumerador, nuevoDenominador);
  }

  public void imprimir() {
    System.out.println(this.numerador + "/" + this.denominador);
  }

  public static void main(String [] args) {
    Racional r1 = new Racional(1,2);
    Racional r2 = new Racional(1,4);

    Racional r3 = r1.sumar(r2);
    r3.imprimir();
  }
}
