public class Calculadora {

  public int sumar(int a, int b) {
    return a + b;
  }

  public int restar(int a, int b) {
    return a - b;
  }

  public int multiplicar(int a, int b) {
    return a * b;
  }

  public int potencia(int numero, int exponente) {
    int potencia = numero;
    for(int i = 2; i <= exponente; i++) {
      potencia *= numero; // numero = numero * numero;
    }

    return potencia;
  }

  public static void main(String [] args) {
    Calculadora c = new Calculadora();
    int resultadoSuma = c.sumar(10, 5);
    int resultadoResta = c.restar(8, 2);
    int resultadoMultiplicacion = c.multiplicar(10,10);
    int resultadoPotencia = c.potencia(2,8);
    System.out.println(resultadoSuma);
    System.out.println(resultadoResta);
    System.out.println(resultadoMultiplicacion);
    System.out.println(resultadoPotencia);
  }
}
