public class Persona {
  private String nombre;
  private String apellidoPaterno;
  private String apellidoMaterno;

  // 1. Hagan sus mutadores GET Y SET
  // 2. Constructor que reciba nombre y apellidos
  // 3. Un método que me regrese el nombre completo

  public Persona(String n, String apP, String apM) {
    this.nombre = n;
    this.apellidoPaterno = apP;
    this.apellidoMaterno = apM;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nuevoNombre) {
    this.nombre = nuevoNombre;
  }

  public String getApellidoPaterno() {
    return this.apellidoPaterno;
  }

  public String getNombreCompleto() {
    return this.nombre + " " + this.apellidoPaterno + " " + this.apellidoMaterno;
  }

  public static void main(String [] args) {
    Persona p = new Persona("Tania", "Ruiz", "Ponce");
    System.out.println(p.getNombreCompleto());
  }
}
