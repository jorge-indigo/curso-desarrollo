public class CuentaBancariaTest {
  public static void main(String [] args) {
    CuentaBancaria c = new CuentaBancaria();
    System.out.println("La cuenta c tiene saldo inicial: " + c.getSaldo());

    c.abonar(200);
    System.out.println("La cuenta c después de abonar 200 es: " + c.getSaldo());

    c.retirar(20);
    System.out.println("La cuenta c después de retirar 20 es: " + c.getSaldo());

    c.retirar(10000);
    System.out.println("La cuenta c después de retirar 10000 es: " + c.getSaldo());


    CuentaBancaria d = new CuentaBancaria(1000);
    d.abonar(10);
    d.retirar(25);
  }
}
