public class Dulce {

  // Atributos
  private int id;
  private String nombre;
  private String tamaño;
  private String sabor;
  private String color;
  private float precio;
  private int descuento;

  // new Dulce()
  public Dulce(int id) {
    this.id = id;
  }

  // new Dulce(String)
  public Dulce(int id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }

  // new Dulce(String, float)
  public Dulce(int id, String nombre, float precioInicial) {
    this.id = id;
    this.nombre = nombre;
    this.precio = precioInicial;
  }

  // new Dulce(String, String, String, String, float)
  public Dulce(int id, String nombre, String tamaño, String sabor, String color, float precio) {
    this.id = id;
    this.nombre = nombre;
    this.tamaño = tamaño;
    this.sabor = sabor;
    this.color = color;
    this.precio = precio;
  }

  // Mutadores
  // GETTERS y SETTERS
  public int getId() {
    return this.id;
  }

  public String getNombre() {
    return this.nombre;
  }

  // No van a poder hacer:
  // dulce.nombre = "algo"; PROHIBIDO
  // dulce.setNombre("algo");
  public void setNombre(String nuevoNombre) {
    this.nombre = nuevoNombre;
  }

  public String getTamaño() {
    return this.tamaño;
  }

  // dulce.tamaño = "super grande"; PROHIBIDO
  // dulce.setTamaño("super grande");
  public void setTamaño(String nuevoTamaño) {
    if (nuevoTamaño.equals("chico") || nuevoTamaño.equals("mediano") || nuevoTamaño.equals("grande")) {
      this.tamaño = nuevoTamaño;
    }
  }

  public float getPrecio() {
    System.out.println("Descuento " + this.descuento);
    if (this.descuento > 0) {
      // 100f
      // 100d
      // 100l
      // 100
      float descuentoReal = this.descuento/100f;
      return this.precio - (this.precio * descuentoReal);
    } else {
      return this.precio;
    }
  }

  // dulce.precio = -1; PROHIBIDO
  // dulce.setPrecio(-1);
  public void setPrecio(float nuevoPrecio) {
    if (nuevoPrecio > 0 && nuevoPrecio > this.precio) {
      this.precio = nuevoPrecio;
    }
  }

  public String getColor() {
    return this.color;
  }

  public void setColor(String nuevoColor) {
    this.color = nuevoColor;
  }

  public String getSabor() {
    return this.sabor;
  }

  public void setSabor(String nuevoSabor) {
    this.sabor = nuevoSabor;
  }

  public int getDescuento() {
    return this.descuento;
  }

  public void setDescuento(int nuevoDescuento) {
    if (nuevoDescuento > 0 && nuevoDescuento <= 100) {
      this.descuento = nuevoDescuento;
    }
  }
}
