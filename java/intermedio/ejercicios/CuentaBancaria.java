public class CuentaBancaria {

  private double saldo;

  // [nivel de acceso] [nombre de la clase]([parametros]){ }
  public CuentaBancaria(double saldoInicial) {
    this.saldo = saldoInicial;
  }

  public CuentaBancaria() {
    this.saldo = 0;
  }

  // [nivel de acceso] [tipo de retorno] [nombre del metodo]( [parametros] ) { }
  public void abonar(double abono) {
    this.setSaldo(this.saldo + abono);
  }

  public void retirar(double retiro) {
    if (retiro <= this.saldo) {
      this.setSaldo(this.saldo - retiro);
    } else {
      System.out.println("No tienes saldo suficiente");
    }
  }

  // Mutadores
  // metodos GET que obtienen el valor de los atributos
  // metodos SET que establecen un nuevo valor
  public double getSaldo() {
    return this.saldo;
  }

  private void setSaldo(double nuevoSaldo) {
    this.saldo = nuevoSaldo;
  }
}

