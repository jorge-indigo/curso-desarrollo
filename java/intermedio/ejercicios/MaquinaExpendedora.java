public class MaquinaExpendedora {

  private int capacidad;
  private Dulce [] dulces;

  public MaquinaExpendedora(int capacidadInicial) {
    this.capacidad = capacidadInicial;
    this.dulces = new Dulce[ capacidadInicial ];

    for(int i = 0; i < this.capacidad; i++) {
      this.dulces[i] = new Dulce((i + 1) * 10, "Carlos V", 10);
    }
  }

  // Vender dulces
  public Dulce vender(int id, float dinero) {
    for(int i = 0; i < this.capacidad; i++) {

      Dulce dulceActual = this.dulces[i];

      if (dulceActual != null && dulceActual.getId() == id) {
        this.dulces[i] = null;
        return dulceActual;
      }
    }

    return null;
  }

  // Llenar la maquina

  // [ o, null, o, o ]
  public static void main(String [] args) {

    MaquinaExpendedora maquinita = new MaquinaExpendedora(10);
    Dulce chocolate = maquinita.vender(20, 0);
    Dulce chocolate2 = maquinita.vender(40, 0);
  }
}

