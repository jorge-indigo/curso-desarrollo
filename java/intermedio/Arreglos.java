public class Arreglos {
  public static void main(String [] args) {
    // [tipo] [] [nombre] = new [tipo][longitud];

    // Un arreglo de 10 posiciones de enteros
    int [] numeritos = new int[5];
    numeritos[0] = 20;
    numeritos[1] = 100;
    numeritos[2] = 80;
    numeritos[3] = 10000;
    numeritos[4] = 9013098;

    for(int i = 0; i < numeritos.length; i++) {
      System.out.println("numeritos[" + i + "] es " + numeritos[i]);
    }
  }
}
