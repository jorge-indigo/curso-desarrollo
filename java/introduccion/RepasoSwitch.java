public class RepasoSwitch {

  public static void main(String [] args) {

    /*
     * Sintaxis del Switch
     *
     * switch( variable ) {
     *
     *  case valor1:
     *    instruccion;
     *    instruccion;
     *    break;
     *
     *  case valor2:
     *    instruccion;
     *    break;
     *
     *  default:
     *    instruccion;
     *    instruccion;
     * }
     */

    int edad = 15;

    if (edad == 0) {
      System.out.println("Es un recien nacido");
    } else if (edad == 2) {
      System.out.println("Los terribles 2");
    } else if (edad == 15) {
      System.out.println("Es un puberto :v");
    } else if (edad == 18) {
      System.out.println("Ya es un adulto");
    } else {
      System.out.println("Es un anciano");
    }

    switch( edad ) {
      case 0:
        System.out.println("Es un recien nacido");
        break;

      case 2:
        System.out.println("Los terribles 2");
        break;

      case 15:
        System.out.println("Es un puberto :v");
        break;

      case 18:
        System.out.println("Ya es un adulto");
        break;

      default:
        System.out.println("Es un anciano");
    }
  }
}
