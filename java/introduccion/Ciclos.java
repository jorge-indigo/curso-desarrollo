public class Ciclos {
  public static void main(String [] args) {
    for(int i = 0; i < 10; i++){
      System.out.println("Iteracion numero: " + i);
    }
    
    int  j = 0;
    while(j < 10) {
      System.out.println("Iteracion while numero: " + j);
      System.out.println("El valor de j es " + j);
      
      break; // Romple el ciclo
    }
    
    /*
     j++; // Regresa el valor y después incrementa uno
     ++j; // Primero incrementa uno y después regresa el valor
     j += 10; // j = j + 10
    */
    j = 10;
    int a = 2;
    a = a + j++;
    System.out.println("a vale " + a);
    
    for(int i = 0; i < 1000000; i++) {
      System.out.println("i vale " + i);
      if (i == 10) {
        break;
      }
      //break; // Romple el ciclo
    }
    
    for(int i = 0; i < 10; i++){
      System.out.println("Nuevamente i vale " + i);
      if (i%2 == 0) {
        continue;
      }
        System.out.println("i es impar");
      
      // ....
    }
  }
}
