public class Sentencias {
    public static void main(String [] args) {
        boolean isStudent = false;
        boolean isGay = false;
        int age = 17;

        /* if ( condicion booleana ) { ... } else { ... }
        condiciones booleana es una expresión que regrese true o false
        por ejemplo
        x > 0 (comparacion de mayor), x == y (igual), !x (negación), x (una variable) */
        if ( isStudent == true ) {
            System.out.println("Hola, no soy un estudiante");
        }
        // If anidado 
        else if ( isGay )  {
            System.out.println("Hola, soy gay");
        }
        else if ( age > 18 ) {
            System.out.println("Hola, tengo 18");
        } 
        else if (1 == 0) {
            System.out.println("Uno es igual a uno, duh");
        }/*
        else {
            System.out.println("Hola, soy otra cosa");
        }*/

        if (isStudent) {
            System.out.println("Hola, soy estudiante 2");
        }

        if (isGay) {
            System.out.println("Hola, soy gay 2");
        }
    }
}