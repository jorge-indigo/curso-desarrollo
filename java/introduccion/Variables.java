public class Variables {
    public static void main(String [] args) {
        // Puedo escribir lo que sea

        /*
        Un comentario
        de
        varias
        lineas
        */

        // Tipos de datos primitivos
        // tipo nombre = valor;
        // tipo nombre;
        
        // BOOLEANO
        // boolean (1 bit) true, false [valor por default: false]

        // NUMEROS ENTEROS [valor por default: 0]
        // byte (8 bits) -128 a 127 
        // short (16 bits) -32768 hasta 32767, valor por default
        // int (32 bits) -2,147,483,648 hasta 2,147,483,647
        // long (64 bits) -9,223,372,036,854,775,808 hasta 9,223,372,036,854,775,807

        // NUMEROS DECIMALES [valor por default: 0]
        // float (32 bits) 1.17549435e-38 hasta 3.4028235e+38
        // double (64 bits) 4.9e-324 hasta 1.7976931348623157e+308
 
        // CARACTERES [valor por default: 0]
        // char (16 bits)

        int age = 0;
        boolean isStudent = false;
        char gender = 'F';
        double money = 1345.5678;

        byte algo;

        algo = 127;

        System.out.println(algo);
    }
}