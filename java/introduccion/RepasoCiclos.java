public class RepasoCiclos {

  public static void main(String [] args) {

    /*
     * Iteraciones
     * 1. Sabemos cuántas veces lo vamos a hacer (FOR)
     *  "Desde 1 hasta 10"
     *  "Desde 20 hasta 100"
     *  "Repetir 5 veces (1 a 5)"
     *
     *  2. No sabemos cuántas, pero tenemos una condición booleana (WHILE)
     *    "Hasta que mi variable sea 10"
     *    "Hasta que mi variable sea par"
     */

    /*
     * Sintaxis
     * for(int i = valor; condicion booleana; incremental ) {
     *  instrucciones;
     * }
     *
     * for(int i = 1; i <= 5; i++) {
     *
     * }
     *
     * En mi primer iteracion i vale 1
     *  * Se cumple i sea menor que 5? Si
     * En mi segunda iteracion i vale 2
     *  * Se cumple i sea menor que 5? Si
     * En mi tercera iteracoin i vale 3
     * ...
     * En mi quinta iteracion i vale 5
     *  * Se cumple la condicion ? Si
     * En mi sexta iteracion i vale 6
     *  * Se cumple, no
     */
    /**
     * Ejecución es:
     * 1. i lo iguala a 6
     *
     * 2. Revisa si se cumple la condición
     *
     * 3. Si se cumple
     *   3.1 Ejecuta todas las instrucciones que esten estre las llaves
     *   3.2 Aumenta en uno a i
     *   3.3 Se repite el paso 2
     *
     * 4. Si no se cumple
     *   4.1 Termina
     */
    for(int i = 6; i <= 5; i++) {
      System.out.println("i vale " + i);
    }

    for(int j = 0; j <= 10; j++) {
      if(j == 8) {
        //break; // Rompe totalmente el ciclo
      } else {
        System.out.println("j es menor que 8, por que vale: " + j);
      }
    }

    /*
     * Sintaxis del while
     *
     * while( condicion booleana para ENTRAR al while ) {
     *  instrucciones;
     * }
     *
     * do {
     *  instrucciones;
     * } while( condiciones booleana para REPETIR el while );
     *
     * El while lo ejecuta SOLAMENTE SI CUMPLE LA CONDICIÓN BO0LEANA
     *
     * El do-while lo ejecuta AL MENOS UNA VEZ y se continuará ejecutando si la condición se cumple
     */
    int i = 4;
    while(i <= 5) {
      System.out.println("Infinito");
      i++;
    }

    do {
      System.out.println("i en el while vale: " + i);
      i++;
    } while(i <= 5);
  }
}
