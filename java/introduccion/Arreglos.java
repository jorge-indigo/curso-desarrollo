public class Arreglos {
  public static void main(String [] args) {
    // tipo [] nombre = new tipo[ numero ];
    int [] numeritos = new int[10];
    
    System.out.println("El arreglo mide " + numeritos.length);
    
    // [ _ , _ , _ , ... , _ ] : Mide N 
    //   0   1   2         N - 1
    
    numeritos[0] = 100;  // [ 100 , 0 , 0 , 0 , 0 , 0 , 0  , 0 , 0 , 0 ]
    numeritos[1] = 500;  // [ 100 , 500 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ]
    numeritos[8] = 200;  // [ 100 , 500 , 0 , 0 , 0 , 0 , 0  , 0 , 200 , 0 ]
    
    for(int i = 0; i < numeritos.length; i++) {
      System.out.println("En la  posicion " + i + " esta el valor " + numeritos[i]);
    }
    
    int [] otrosNumeritos = new int [] { 1, 2 , 3 };
    /*
     int []  otrosNumeritos = new int [3];
     otrosNumeritos[0] = 1;
     otrosNumeritos[1] = 2;
     otrosNumeritos[2] = 3;
     */
  }
}