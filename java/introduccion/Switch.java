public class Switch {

    public static void main(String [] args) {

        /*
        switch( variable ) {

            case valor:
            case valor2:
            case valor3:
        }
        */
        int age = 17;

        /*if (age >= 11 && age <= 16 ) {
            System.out.println("No puedes crear tu cuenta de Facebook");
        }
        else if (age < 16) {
            System.out.println("No puedes tener una licencia de conducir");
        }
        else if(age < 18) {
            System.out.println("No puedes votar");
        }
        else {
            System.out.println("Eres viejo");
        }*/

        switch(age) {
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                System.out.println("No puedes tener una licencia de conducir");
            case 17:
            case 18:
                System.out.println("No puedes votar");
                break;
            default:
                System.out.println("Eres viejo");
        }

        String gender = "M";
        switch(gender) {
            case "M":
                System.out.println("Es masculino");
                break;
            case "F":
                System.out.println("Es femenino");
                break;
            default:
                System.out.println("No pos quien sabe");
        }
    }
}