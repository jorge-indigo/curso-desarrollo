import java.util.Scanner;

public class GradosGringos {

  public static void main(String [] args){
    Scanner scanner = new Scanner (System.in);

    System.out.println ("¿Qué valor quieres convertir?");
    System.out.println ("A+");
    System.out.println ("A");
    System.out.println ("A-");
    System.out.println ("B+");
    System.out.println ("B");
    System.out.println ("B-");
    System.out.println ("C+");
    System.out.println ("C");
    System.out.println ("C-");
    System.out.println ("D+");
    System.out.println ("D");
    System.out.println ("D-");
    System.out.println ("F");
    System.out.println ("F-");

    String respuesta = scanner.nextLine ();

    switch(respuesta) {

      case "A+":
        System.out.println("El valor es 4.25");
        break;

      case "A":
        System.out.println("El valor es 4.0");
        break;

      case "A-":
        System.out.println("El valor es 3.75");
        break;

      case "B+":
        System.out.println("El valor es 3.25");
        break;

      case "B":
        System.out.println("El valor es 3.0");
        break;

      case "B-":
        System.out.println("El valor es 2.75");
        break;

      case "C+":
        System.out.println("El valor es 2.25");
        break;

      case "C":
        System.out.println("El valor es 2.0");
        break;

      case "C-":
        System.out.println("El valor es 1.75");
        break;

      case "D+":
        System.out.println("El valor es  1.25");
        break;

      case "D":
        System.out.println("El valor es 1.0");
        break;

      case "D-":
        System.out.println("El valor es 0.75");
        break;

      case "F":
        System.out.println("El valor es 0.25");
        break;

      case "F-":
        System.out.println("El valor es 0");
        break;

      default:
        System.out.println("Disculpa, ese valor no existe.");
    }
  }
}
