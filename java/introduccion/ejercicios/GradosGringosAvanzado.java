import java.util.Scanner;

public class GradosGringosAvanzado {

  public static void main(String [] args) {

    /**
     * A+   4.25
     * A    4.0
     * A-   3.75
     * B+   3.25
     * B    3.0
     * B-   2.75
     * C+   2.25
     * C    2.0
     * C-   1.75
     * D+   1.25
     * D    1.0
     * D-   0.75
     * F    0.25
     * F-   0
     */

    Scanner sc;
    String grade;
    double gradeValue = 0;
    double gradeTotal = 0;
    int noGrades = 0;

    System.out.println("¿Cuántas calificaciones va a ingresar?");
    sc = new Scanner(System.in);
    noGrades = sc.nextInt();

    for(int i = 0; i < noGrades; i++) {
      sc = new Scanner(System.in);
      System.out.println("Ingrese la calificación #" + (i + 1) + ":");
      grade = sc.nextLine();

      switch(grade) {
        case "A+":
          gradeValue = 4.25;
          break;

        case "A":
          gradeValue = 4.0;
          break;

        case "A-":
          gradeValue = 3.75;
          break;

        case "B+":
          gradeValue = 3.25;
          break;

        case "B":
          gradeValue = 3.0;
          break;

        case "B-":
          gradeValue = 2.75;
          break;

        case "C+":
          gradeValue = 2.25;
          break;

        case "C":
          gradeValue = 2.0;
          break;

        case "C-":
          gradeValue = 1.75;
          break;

        case "D+":
          gradeValue = 1.25;
          break;

        case "D":
          gradeValue = 1.0;
          break;

        case "D-":
          gradeValue = 0.75;
          break;

        case "F":
          gradeValue = 0.25;
          break;

        case "F-":
          gradeValue = 0;
          break;

        default:
          gradeValue = 0;
          System.out.println("Calificación inválida");
      }

      System.out.println("La calificación es: " + gradeValue);
      gradeTotal += gradeValue;
    }

    System.out.println("El promedio del alumno es: " + (gradeTotal/noGrades));
  }
}
