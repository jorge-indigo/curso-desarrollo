public class Swap {
  public static void main (String ... args) {

    /**
     * Solución de Alan
     *
    int b = 20;
    int c = 30;
    System.out.println("El valor de b es " + b);
    System.out.println("El valor de c es " + c);
    int temp = c;
    c = b;
    b = temp;
    System.out.println("El valor de b es " + b);
    System.out.println("El valor de c es " + c);
    */

    /**
     * Solución de Sam
     */
    int a= 20;
    int b= 30;
    System.out.println ("El valor de a y b antes de swapearlo, a: " + a +" b: " + b);
    a = a + b;
    b = a -b;
    a = a -b;
    System.out.println ("El valor de a y b después de swapearlo, a: " + a +" b: " + b);
  }
}
