import java.util.Scanner;

public class Cuestionario {

  public static void main(String [] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.println("¿Cuánto es 1 + 1?");
    System.out.println("a. 3");
    System.out.println("b. 5");
    System.out.println("c. 4");
    System.out.println("d. 2");

    String respuesta = scanner.nextLine();

    switch(respuesta){
      case "d":
        System.out.println("Correcto");
        break;

      case "b":
        System.out.println("Que vergüenza");
        break;

      case "c":
        System.out.println("terrible");
        break;

      case "a":
        System.out.println("???");
        break;

      default:
        System.out.println("Por favor entrega tu examen estás reprobado...");
    }
  }

}
