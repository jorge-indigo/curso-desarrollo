public class PositivoONegativo {
  public static void main(String [] args) {
    /*
     * Dada una variable x
     * Si x es mayor a 0, entonces imprimir "X ES POSITIVO"
     * Si x es menor a 0, entonces imprimir "X ES NEGATIVO"
     * Si x es 0, entonces imprimir "X ES CERO"
     */
    int x = 0;

    if(x > 0) {
      System.out.println("X es positivo");
    } else if(x < 0) {
      System.out.println("X es negativo");
    } else {
      System.out.println("X es cero");
    }
  }
}
