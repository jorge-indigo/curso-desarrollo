import java.util.Scanner;

public class Arbolito {
  public static void main(String [] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Nivel del arbol");
    int niveles = sc.nextInt();
    
    for(int i = 1; i <= niveles; i++){
      
      String espaciosBlancos =  "";
      for(int j = niveles - i; j >= 1; j--) {
        espaciosBlancos += " ";
      }
      
      String asteriscos = "*";
      for(int j = 2; j <= i; j++) {
        asteriscos = "*" + asteriscos + "*";
      }
      
      System.out.println(espaciosBlancos + asteriscos);
    }
  }
}