import java.util.Scanner;
public class MenuComida{
  public static void main(String [] args){
    boolean terminar = true;
    while (terminar){
      Scanner entrada = new Scanner(System.in);
      System.out.println("Bienvenido, por favor seleccione una opcion del menu");
      System.out.println("1.- Hamburguesa de queso con papas");
      System.out.println("2.- Pizza de queso");
      System.out.println("3.- Ensalada");
      System.out.println("4.- Tacos");
      System.out.println("5.- Sushi");
      System.out.println("6.- Salir");
      int opcion = entrada.nextInt();
      switch (opcion){
        case 1:
          System.out.println("Aqu� est� tu hamburguesa");
          break;
        case 2:
          System.out.println("Aqu� tienes tu pizza");
          break;
        case 3:
          System.out.println("Aqu� tienes tu ensalada");
          break;
        case 4:
          System.out.println("Aqu� tienes tus tacos");
          break;
        case 5:
          System.out.println("Tu sushi ya est� listo");
          break;
        case 6:
          System.out.println("Adios... por favor pase a caja a pagar sus alimentos");
          terminar = false;
          break;
        default:
          System.out.println("Opci�n no valida... por favor elige nuevamente");
          break;
      }
      entrada = new Scanner(System.in);
      if (opcion >=1 && opcion <= 5){
        System.out.println("A que domicilio se llevar� el pedido");
        String domicilio = entrada.nextLine();
        System.out.println("El paquete numero"+ opcion + "ser� llevado a" + domicilio);
        if (domicilio.contains ("ecatepec")){
          System.out.println("No llegamos a esa zona");
        }
        else  {
          System.out.println("quieres pedir otro alimento?");
          entrada = new Scanner(System.in);
          String opcion1 =entrada.nextLine();
          switch (opcion1) {
            case "Si":
              System.out.println("Si");
              break;
            case "No":
              System.out.println("No");
              terminar = false;
              break;
            default:
              System.out.println("Opcion no valida");
              break;
          }
        }
      }
    }
  }
}