public class MinimoArreglo {
  public static void main(String [] args) {
    int [] numeritos = new int [] { 0, -500, 2, 40, -1000, 1 };
    
    int minimo = numeritos[0];
    for(int i = 0; i < numeritos.length; i++) {
      if (minimo > numeritos[i]) {
        minimo = numeritos[i];
      }
    }
    
    System.out.println("El minimo es " + minimo);
  }
}