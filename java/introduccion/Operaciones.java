public class Operaciones {
    public static void main(String [] args) {
        int entero1 = 5;
        int entero2 = 2;

        // Suma
        int suma = entero1 + entero2;

        // Resta
        int resta = entero1 - entero2;

        // Multiplicación
        int multiplicacion = entero1 * entero2;

        // Division
        double division = entero1 / entero2;

        // Módulo
        int modulo = entero1 % entero2;

        System.out.println("Division de 5/2 es");
        System.out.println(division);
    }
}