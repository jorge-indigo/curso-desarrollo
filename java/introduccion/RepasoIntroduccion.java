public class RepasoIntroduccion {
  public static void main(String [] args) {
    System.out.println("Hello World!");
    System.out.println("Los odio a todos");

    // Esto es un comentario
    /*
     * Una variable es espacio en memoria que se le puede asignar un valor
     *
     * Tipos de variables primivitos
     * boolean (true, false)
     * int (Un número entero de 32 bits)
     * float (un número con punto decimal de 32 bits)
     * double (Un número con punto decimal de 64 bits)
     * short (Un número entero de 16bits)
     * char (Un caracter)
     * long (Un número entero de 64 bits)
     * byte (Un número entero de 8 bits)
     */
    // [tipo] [nombre a la variable] = [valor];
    // [tipo] [nombre a la variable];

    byte estoEsUnByte = 256;
    int estoEsUnEntero = 1500;
    char estoEsUnCaracter = 'z';
    double estoEsUnDouble = 27.69;

    // No es primitivo
    String estoEsUnaCadena = "Hola Mundo";
  }
}
