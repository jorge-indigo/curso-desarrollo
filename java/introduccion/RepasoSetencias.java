public class RepasoSetencias {
  public static void main(String [] args) {
    /* Expresiones booleanas */
    // Boolean valores: true (verdades o 1) o false (falso o 0), default: false
    // Operaciones

    // Y lógico (&&)
    // La expresión es verdadera, si todas las variables son verdaderas
    // La expresión es falsa, si alguna es falsa
    // true && true && true = true (porque todas son true)
    // true && true && false = false (por que la última es false)
    // false && false = false (por que desde la primera es false)

    // O lógico (||)
    // La expresión es verdadera, si alguna es verdadera
    // La expresión es falsa, si todas son falsas
    // true || true = true (porque al menos una es true)
    // false || true = true (porque al menos una es true)
    // false || false = false (por que todas son false)

    // >
    // <
    // ==, !=
    // >=
    // <=

    if (/* expresion booleana */ true && true && false) {
      System.out.println("La expresión es verdadera");
    }

    if ( (false || false ) && true ) {
      System.out.println("La expresión es verdadera");
    } else {
      System.out.println("La expresión es falsa");
    }

    int x = 1;

    if (x < 10) {
      System.out.println("X es menor a 10");
    } else if (x > 0 && x < 10) {
      System.out.println("X esta en un rango de 1 a 9");
    } else if (x > 10) {
      System.out.println("X es mayor que 10");
    } else {
      System.out.println("X es igual a 10");
    }

    if (x < 10) {
      System.out.println("X es menor a 10 (v2)");
    }

    if (x > 0 && x < 10) {
      System.out.println("X esta entre 1 a 9 (v2)");
    }

    /*
     *  int i = 0;
     *  int j = -1;
     *
     *
     *  if(j > 0 ) {
     *    i = 10;
     *
     *    if (..) {
     *      i = 15;
     *    }
     *    else {
     *     i = 0;
     *    }
     *  } else {
     *    i = 20
     *  }
     *
     *  int i = (true && false) ? ((false || true) ? 15 : 0 ): 20;
     *
     */
  }
}
