import java.util.Scanner; // Importar la clase Scanner del paquete java.util (java/util/Scanner.java)

public class RepasoScanner {

  public static void main(String [] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.println("¿Me quieres?");

    /**
     * nextInt - Captura un entero (int)
     * nextDouble - Captura un numero decimal (double)
     * nextLine - Captura una linea completa (String)
     */
    String linea = scanner.nextLine();

    /*if (linea.equals("si")) {
      System.out.println("<3");
    } else {
      System.out.println("</3");
    }*/

    switch(linea) {

      case "si":
      case "Si":
      case "sI":
      case "SI":
      case "S":
        System.out.println("<3");
        break;

      default:
        System.out.println("</3");
    }
  }
}
